package com.kasrat.repo;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.apache.cassandra.utils.UUIDGen;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.kasrat.entity.WorkoutPlanEntity;
import com.kasrat.repo.cassandra.WorkoutPlanRepo;
import com.kasrat.repo.solr.WorkoutPlanSearchRepo;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( locations = {"classpath:/META-INF/kasrat-context-test.xml"})
public class WorkoutPlanReportTest {

	@Autowired
	protected WorkoutPlanRepo wpRepo;

	@Autowired 
	protected WorkoutPlanSearchRepo workoutPlanSearchRepo;
	
	@Test
	public void testWorkoutPlanSave() {
		WorkoutPlanEntity wpe = new WorkoutPlanEntity();
		
		UUID planId = UUIDGen.getTimeUUID();
		wpe.setPlanId(planId);
		wpe.setPlanName("Vijays workout");
		wpe.setSportName("Body building");
		wpe.setCreatedBy("Unit Test User");
		wpe.setSummary("Lean muscle workout");
		Set<String> tags = new HashSet<String>();
		tags.add("lean");
		tags.add("ripped");
		tags.add("core");
		wpe.setTags(tags);
		wpe.setDetails("Arnords worokout");
		WorkoutPlanEntity entity  = wpRepo.save(wpe);
		
		Pageable pg = new PageRequest(0, 20);
		List<WorkoutPlanEntity> searchRes = workoutPlanSearchRepo.findWorkoutsByText("test", pg).getContent();
		System.out.println("Total results" + searchRes.size());
	}
}

