package com.kasrat.repo;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.cassandra.exceptions.ConfigurationException;
import org.apache.thrift.transport.TTransportException;
import org.cassandraunit.utils.EmbeddedCassandraServerHelper;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.cassandra.config.java.AbstractCassandraConfiguration;
import org.springframework.data.cassandra.repository.config.EnableCassandraRepositories;

import com.datastax.driver.core.Cluster;

@Configuration
@EnableCassandraRepositories(basePackages = "com.kasrat.repo" )
public class CassandraUnitTestConfig extends AbstractCassandraConfiguration {

	private static boolean cqlPopulated = false;
/*	static {
		try {
			EmbeddedCassandraServerHelper.startEmbeddedCassandra();
			EmbeddedCassandraServerHelper.cleanEmbeddedCassandra();			
			Cluster.builder().addContactPoint("127.0.0.1").withPort(9042).build();
		} catch(ConfigurationException | TTransportException | IOException | InterruptedException e) {
			e.printStackTrace();
		}
	}
	*/
	
	@Override
	protected String getKeyspaceName() {
		return "kasrat";
	}

	@Override
	protected String getContactPoints() {
		return "127.0.0.1";
	}

	@Override
	protected int getPort() {
		return 9042;
	}
	
	@Override
	protected List<String> getStartupScripts() {
		List<String> list = new ArrayList<String>();
		BufferedReader sqlReader = null;
		if(cqlPopulated) {
			return list;
		}
		
		try {
			
			sqlReader = new BufferedReader(new FileReader("/Users/vijaypawnarkar/athelete/coachmeup/restful/src/main/schema/coachmeup.cql"));
			String sql = null;
			while((sql = sqlReader.readLine()) != null) {
				sql = sql.trim();
				if(sql.length() > 0) {
					list.add(sql);
				}
			}
			cqlPopulated = true;
		} catch(IOException ex) {
			ex.printStackTrace();
		}
		finally {
			if(sqlReader != null) {
				try {
					sqlReader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}		
		return list;
	}
}
