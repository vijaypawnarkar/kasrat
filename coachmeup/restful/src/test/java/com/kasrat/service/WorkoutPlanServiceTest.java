package com.kasrat.service;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.kasrat.common.SearchParameter;
import com.kasrat.entity.WorkoutPlanEntity;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( locations = {"classpath:/META-INF/kasrat-context-test.xml"})
public class WorkoutPlanServiceTest {

	@Autowired
	protected WorkoutPlanService workoutPlanService;
	
	@Test
	public void testWorkoutPlanService() {
		SearchParameter param = new SearchParameter();
		param.setSearchString("Test");
		List<WorkoutPlanEntity> searchRes = workoutPlanService.findWorkoutPlans(param);
		System.out.println("Total results" + searchRes.size());

	}
}
