package com.kasrat.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.cassandra.config.java.AbstractCassandraConfiguration;
import org.springframework.data.cassandra.repository.config.EnableCassandraRepositories;

@Configuration
@EnableCassandraRepositories(basePackages = {"com.kasrat.repo.cassandra"})
public class CassandraRepoConfig extends AbstractCassandraConfiguration{
	
	@Value("${cassandra.keyspace}")
	private String keyspace;

	@Value("${cassandra.port}")
	private int cassandraPort;
	
	@Value("${cassandra.host}")
	private String cassandraHost;
	
	@Override
	public int getPort() {
		return cassandraPort;
	}
	
	@Override
	public String getContactPoints() {
		return cassandraHost;
	}
	
	@Override
	public String getKeyspaceName() {
		return keyspace;
	}

	
}
