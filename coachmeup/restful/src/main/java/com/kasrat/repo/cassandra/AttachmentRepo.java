package com.kasrat.repo.cassandra;

import java.util.UUID;

import org.springframework.data.cassandra.repository.TypedIdCassandraRepository;

import com.kasrat.entity.AttachmentEntity;

public interface AttachmentRepo extends TypedIdCassandraRepository<AttachmentEntity, UUID> {

}
