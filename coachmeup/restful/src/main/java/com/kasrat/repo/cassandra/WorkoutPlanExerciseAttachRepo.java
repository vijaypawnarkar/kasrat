package com.kasrat.repo.cassandra;

import java.util.UUID;

import org.springframework.data.cassandra.repository.TypedIdCassandraRepository;

import com.kasrat.entity.WorkoutPlanExerciseAttachEntity;


public interface WorkoutPlanExerciseAttachRepo extends TypedIdCassandraRepository<WorkoutPlanExerciseAttachEntity, UUID>{

}
