package com.kasrat.repo.cassandra;

import java.util.List;

import org.springframework.data.cassandra.repository.Query;
import org.springframework.data.cassandra.repository.TypedIdCassandraRepository;

import com.kasrat.entity.UserWorkoutPlanEntity;

public interface UserWorkoutPlanRepo extends TypedIdCassandraRepository<UserWorkoutPlanEntity, String>{

	@Query("select * from user_workout_plan where saved_by = ?0 ")
	List<UserWorkoutPlanEntity> getUserWorkoutPlans(String userId);
	
}
