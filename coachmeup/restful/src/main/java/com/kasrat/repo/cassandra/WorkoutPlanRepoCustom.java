package com.kasrat.repo.cassandra;

import java.util.List;
import java.util.UUID;

import com.kasrat.entity.WorkoutPlanEntity;

public interface WorkoutPlanRepoCustom {

	List<WorkoutPlanEntity> getWorkoutsByPlanIdsIn(List<UUID> planIds) ;

}
