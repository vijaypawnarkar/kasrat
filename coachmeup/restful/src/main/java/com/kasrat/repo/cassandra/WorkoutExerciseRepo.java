package com.kasrat.repo.cassandra;

import java.util.List;

import org.springframework.data.cassandra.repository.Query;
import org.springframework.data.cassandra.repository.TypedIdCassandraRepository;

import com.kasrat.entity.WorkoutPlanExerciseEntity;

public interface WorkoutExerciseRepo extends TypedIdCassandraRepository<WorkoutPlanExerciseEntity, String>{

	@Query("select * from workout_plan_exercise where exercise_id = ?0 ALLOW FILTERING")
	public WorkoutPlanExerciseEntity getExercise(String exerciseId);

	@Query("select * from workout_plan_exercise where plan_id = ?0 ")
	public List<WorkoutPlanExerciseEntity> getExercisesByPlan(String planId);

	@Query("select * from workout_plan_exercise where created_by = ?0 ALLOW FILTERING")
	public List<WorkoutPlanExerciseEntity> getExercisesByUser(String userId);

}
