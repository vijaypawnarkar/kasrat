package com.kasrat.repo.solr;



import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.solr.repository.Query;
import org.springframework.data.solr.repository.SolrRepository;

import com.kasrat.entity.WorkoutPlanEntity;


public interface WorkoutPlanSearchRepo  extends SolrRepository<WorkoutPlanEntity, String>{

	@Query(value = "text:?0")
	Page<WorkoutPlanEntity> findWorkoutsByText(String search, Pageable pagable);

}
