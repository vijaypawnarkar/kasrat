package com.kasrat.repo.cassandra;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.cassandra.core.CassandraOperations;
import org.springframework.stereotype.Repository;

import com.kasrat.entity.WorkoutPlanEntity;

@Repository
public class WorkoutPlanRepoImpl implements WorkoutPlanRepoCustom {

	@Autowired @Qualifier("cassandraTemplate")
	CassandraOperations template;
	 
	@Override
	public List<WorkoutPlanEntity> getWorkoutsByPlanIdsIn(List<UUID> planIds) {
		
		StringBuilder planIdList  = new StringBuilder();
		planIdList.append("select * from kasrat.workout_plan where plan_id in (");
		int i = 0;
		for(UUID planId: planIds) {
			planIdList.append(planId.toString());
			i++;
			if(i < planIds.size()) {
				planIdList.append(",");
			}
		}
		planIdList.append(")");
		
		List<WorkoutPlanEntity> plans = template.select(planIdList.toString(), WorkoutPlanEntity.class);
		return plans;
	}

}
