package com.kasrat.repo.cassandra;

import org.springframework.data.cassandra.repository.*;

import com.kasrat.entity.UserEntity;

public interface PersonRepo extends TypedIdCassandraRepository<UserEntity, String> {

}
