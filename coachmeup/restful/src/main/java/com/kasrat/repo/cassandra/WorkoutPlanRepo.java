package com.kasrat.repo.cassandra;

import java.util.List;
import java.util.UUID;

import org.springframework.data.cassandra.repository.Query;
import org.springframework.data.cassandra.repository.TypedIdCassandraRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.kasrat.entity.Constants;
import com.kasrat.entity.WorkoutPlanEntity;

public interface WorkoutPlanRepo extends TypedIdCassandraRepository<WorkoutPlanEntity, UUID>, WorkoutPlanRepoCustom{

	@Query("select * from workout_plan where plan_id = ?0  ")
	List<WorkoutPlanEntity> getWorkoutPlan(UUID planId);

	@Query("select * from workout_plan where created_by = ?0 LIMIT " + Constants.RECORD_LIMIT + " ALLOW FILTERING")
	List<WorkoutPlanEntity> getWorkoutsCreatedByUser(String userId);
	

	
}
