package com.kasrat.repo.cassandra;

import java.util.UUID;

import org.springframework.data.cassandra.repository.TypedIdCassandraRepository;

import com.kasrat.entity.WorkoutPlanAttachEntity;

public interface WorkoutPlanAttachRepo extends TypedIdCassandraRepository<WorkoutPlanAttachEntity, UUID>{

}
