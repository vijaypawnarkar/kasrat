package com.kasrat.repo.solr;

import java.net.MalformedURLException;

import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.impl.HttpSolrServer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.solr.core.SolrTemplate;
import org.springframework.data.solr.repository.config.EnableSolrRepositories;
import org.springframework.data.solr.repository.support.SolrRepositoryFactory;
import org.springframework.data.solr.server.SolrServerFactory;
import org.springframework.data.solr.server.support.MulticoreSolrServerFactory;

@Configuration
@EnableSolrRepositories(basePackages = { "com.kasrat.repo.solr" }, multicoreSupport = true)
public class SolrContext {

	@Value("${solr.url}")
	private String solrUrl;
	
	@Value("${solr.core.workout_plan}")
	private String workoutPlanCore;

	@Bean
	  public SolrServerFactory solrServerFactory() throws MalformedURLException , IllegalStateException {
	    return new MulticoreSolrServerFactory(new HttpSolrServer(solrUrl));
	  }
	
	  @Bean
	  public SolrServer solrServer() throws MalformedURLException , IllegalStateException {
	    return new HttpSolrServer(solrUrl);
	  }
	
	  @Bean
	  public SolrTemplate workoutPlanTemplate() throws Exception {
		  SolrTemplate solrTemplate = new SolrTemplate(solrServerFactory());
		    solrTemplate.setSolrCore(workoutPlanCore);
		    return solrTemplate;
	  }
}