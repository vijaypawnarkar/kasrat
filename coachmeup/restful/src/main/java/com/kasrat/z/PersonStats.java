package com.kasrat.z;

import java.util.Map;

public class PersonStats {

	public static enum StatType {
		CALORIES,
		TOTAL,
		AVERAGEHEART,
		ANNUAL
	}
			
	private String personId;
	private String email;
	private Map<String, Stat> stats;
	
	
	public class Stat {
		String name;
		float value;
		String units;
		
		Stat(String name, float value, String units) {
			this.name = name;
			this.value = value;
			this.units = units;
		}
	}


	public String getPersonId() {
		return personId;
	}


	public String getEmail() {
		return email;
	}


	public Map<String, Stat> getStats() {
		return stats;
	}


	public void setPersonId(String personId) {
		this.personId = personId;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public void setStats(Map<String, Stat> stats) {
		this.stats = stats;
	}
	
	
}
