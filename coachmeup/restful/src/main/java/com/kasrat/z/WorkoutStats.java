package com.kasrat.z;

public class WorkoutStats {

	private static final String WEIGHT="weight";
	private static final String REPEATITIONS="repeatitions";

	private static final String DISTANCE="distance";
	private static final String TIME="time";
	
	private static final String LAPS="laps";
	private static final String LAP_LENGTH ="lap_length";
	
	private static final String ELEVATION_GAIN="elevation_gain";
	private static final String ELEVATION_LOSS="elevation_loss";
	
}
