package com.kasrat.entity;

import java.util.Date;
import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.Table;

import org.springframework.cassandra.core.PrimaryKeyType;
import org.springframework.data.cassandra.mapping.Column;
import org.springframework.data.cassandra.mapping.PrimaryKeyColumn;

@Entity
@Table(name="workout_plan_attach")
public class WorkoutPlanAttachEntity {

	@PrimaryKeyColumn(name = "plan_id", ordinal = 1, type = PrimaryKeyType.PARTITIONED )
	private UUID   planId;
	
	@PrimaryKeyColumn(name = "attachment_id", ordinal = 2, type = PrimaryKeyType.CLUSTERED )
	private UUID attachmentId;
	
	@Column(value = "created_at")
	private Date   createdAt; 

	public UUID getAttachmentId() {
		return attachmentId;
	}
	
	public void setAttachmentId(UUID attachmentId) {
		this.attachmentId = attachmentId;
	}

	public UUID getPlanId() {
		return planId;
	}
	
	public void setPlanId(UUID planId) {
		this.planId = planId;
	}
	
	public Date getCreatedAt() {
		return createdAt;
	}
	
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	
}
