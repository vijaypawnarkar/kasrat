package com.kasrat.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.Id;

import org.apache.solr.client.solrj.beans.Field;
import org.springframework.cassandra.core.PrimaryKeyType;
import org.springframework.data.cassandra.mapping.Column;
import org.springframework.data.cassandra.mapping.PrimaryKeyColumn;
import org.springframework.data.cassandra.mapping.Table;
import org.springframework.data.solr.core.mapping.SolrDocument;

@Entity
@Table(value = "workout_plan")
@SolrDocument(solrCoreName="kasrat.workout_plan")
public class WorkoutPlanEntity implements Serializable {

	@Field("created_by")
	@PrimaryKeyColumn(name="created_by", ordinal=1, type = PrimaryKeyType.CLUSTERED)
	private String createdBy;

	@Field("sport_name")
	@PrimaryKeyColumn(name="sport_name", ordinal = 3, type = PrimaryKeyType.CLUSTERED)
	private String sportName;

	@Id
	private String id;
	
	@Field("plan_id")
	@PrimaryKeyColumn(name="plan_id", ordinal=0, type = PrimaryKeyType.PARTITIONED)
	private UUID planId;
	
	@Field("plan_name")
	@PrimaryKeyColumn(name="plan_name", ordinal=2, type = PrimaryKeyType.CLUSTERED)
	private String planName;
	
	
	@Field("created_at")
	@Column(value = "created_at")
	private Date createdAt;
	
	@Field("summary")
 	@Column(value = "summary")
	private String summary;
	
	@Field("details")
	@Column(value = "details")
	private String details;
	
	@Field("tags")
	@Column(value = "tags")
	private Set<String> tags= new HashSet<String>();

	@Field("level")
	@Column(value = "level")
	private String level;
	
	@Field("access")
	@Column(value = "access")
	private String access;
	
	@Field("plan_details")
	@Column(value = "plan_details")
	private String planDetail;
	
	@Field("cover_image_url")
	@Column(value = "cover_image_url")
	private String coverPictureUrl;
	
	@Field("text")
	private String text;

	public UUID getPlanId() {
		return planId;
	}
	public String getPlanName() {
		return planName;
	}
	
	public String getPlanDetail() {
		return planDetail;
	}
	
	public void setPlanId(UUID planId) {
		this.planId = planId;
	}
	public void setPlanName(String planName) {
		this.planName = planName;
	}
	
	
	public void setPlanDetail(String planDetail) {
		this.planDetail = planDetail;
	}

	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getSummary() {
		return summary;
	}
	public void setSummary(String summary) {
		this.summary = summary;
	}
	public String getDetails() {
		return details;
	}
	public void setDetails(String details) {
		this.details = details;
	}
	public String getSportName() {
		return sportName;
	}
	public void setSportName(String sportName) {
		this.sportName = sportName;
	}
	public Set<String> getTags() {
		return tags;
	}
	public void setTags(Set<String> tags) {
		this.tags = tags;
	}
	public String getLevel() {
		return level;
	}
	public void setLevel(String level) {
		this.level = level;
	}
	public String getCoverPictureUrl() {
		return coverPictureUrl;
	}
	public void setCoverPictureUrl(String coverPictureUrl) {
		this.coverPictureUrl = coverPictureUrl;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Date getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	public String getAccess() {
		return access;
	}
	public void setAccess(String access) {
		this.access = access;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	

}
