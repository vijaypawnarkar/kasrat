package com.kasrat.entity;

public class Constants {
	public static enum EXERCISE_TYPE {
		RUN,
		WALK,
		BIKE,
		SWIM,
		ELLIPTICAL,
		STEPPER,
		WEIGHTS,
		STATIONARY_BIKE,
		ROWING		
	}

	public static final int RECORD_LIMIT = 50;
}
