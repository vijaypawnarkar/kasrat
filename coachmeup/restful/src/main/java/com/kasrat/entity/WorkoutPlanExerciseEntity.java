package com.kasrat.entity;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import javax.persistence.Entity;

import org.apache.solr.client.solrj.beans.Field;
import org.springframework.cassandra.core.PrimaryKeyType;
import org.springframework.data.cassandra.mapping.Column;
import org.springframework.data.cassandra.mapping.PrimaryKeyColumn;
import org.springframework.data.cassandra.mapping.Table;
import org.springframework.data.solr.core.mapping.SolrDocument;

@Entity
@Table(value = "workout_plan_exercise")
@SolrDocument(solrCoreName="kasrat.workout_plan")
public class WorkoutPlanExerciseEntity {

	@Field("sport_name")
	@PrimaryKeyColumn(name="sport_name", ordinal = 0, type = PrimaryKeyType.PARTITIONED)
	private String sportName;
	
	@PrimaryKeyColumn (name ="exercise_id", ordinal = 1, type = PrimaryKeyType.CLUSTERED)
	private UUID exerciseId;
	
	@PrimaryKeyColumn(name="exercise_name", ordinal=2, type = PrimaryKeyType.CLUSTERED)
	private String exerciseName;
	
	@Field("created_by")
	@PrimaryKeyColumn(name="created_by", ordinal=3, type = PrimaryKeyType.CLUSTERED)
	private String createdBy;
	
	@Column(value = "plan_id")
	private UUID planId;
	
	@Field("created_at")
	@Column(value = "created_at")
	private Date createdAt;
	
	@Field("summary")
 	@Column(value = "summary")
	private String summary;
	
	@Field("details")
	@Column(value = "details")
	private String details;
	
	@Field("tags")
	@Column(value = "tags")
	private Set<String> tags= new HashSet<String>();

	@Field("level")
	@Column(value = "level")
	private String level;
	
	@Field("access")
	@Column(value = "access")
	private String access;
	
	@Column (value = "cover_picture_url")
	private String coverPictUrl;

	@Column (value = "cover_video_url")
	private String coverVideoUrl;
	
	public UUID getExerciseId() {
		return exerciseId;
	}
	public void setExerciseId(UUID exerciseId) {
		this.exerciseId = exerciseId;
	}
		
	public String getExerciseName() {
		return exerciseName;
	}
	public void setExerciseName(String exerciseName) {
		this.exerciseName = exerciseName;
	}

	public String getExerciseDetails() {
		return details;
	}
	public void setExerciseDetails(String exerciseDetails) {
		this.details = exerciseDetails;
	}
	
	public String getCoverPictUrl() {
		return coverPictUrl;
	}
	public void setCoverPictUrl(String coverPictUrl) {
		this.coverPictUrl = coverPictUrl;
	}
	
	public String getCoverVideoUrl() {
		return coverVideoUrl;
	}
	public void setCoverVideoUrl(String coverVideoUrl) {
		this.coverVideoUrl = coverVideoUrl;
	}
	public String getSportName() {
		return sportName;
	}
	public void setSportName(String sportName) {
		this.sportName = sportName;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public UUID getPlanId() {
		return planId;
	}
	public void setPlanId(UUID planId) {
		this.planId = planId;
	}
	public Date getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	public String getSummary() {
		return summary;
	}
	public void setSummary(String summary) {
		this.summary = summary;
	}
	public String getDetails() {
		return details;
	}
	public void setDetails(String details) {
		this.details = details;
	}
	public Set<String> getTags() {
		return tags;
	}
	public void setTags(Set<String> tags) {
		this.tags = tags;
	}
	public String getLevel() {
		return level;
	}
	public void setLevel(String level) {
		this.level = level;
	}
	public String getAccess() {
		return access;
	}
	public void setAccess(String access) {
		this.access = access;
	}
}
