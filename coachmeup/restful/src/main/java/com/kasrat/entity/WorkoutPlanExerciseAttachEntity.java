package com.kasrat.entity;

import java.util.Date;
import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.Table;

import org.springframework.cassandra.core.PrimaryKeyType;
import org.springframework.data.cassandra.mapping.Column;
import org.springframework.data.cassandra.mapping.PrimaryKeyColumn;

@Entity
@Table(name="workout_plan_exercise_attach")
public class WorkoutPlanExerciseAttachEntity {

	@PrimaryKeyColumn(name = "exercise_id", ordinal = 1, type = PrimaryKeyType.PARTITIONED )
	private UUID   exerciseId;
	
	@PrimaryKeyColumn(name = "attachment_id", ordinal = 2, type = PrimaryKeyType.CLUSTERED )
	private UUID attachmentId;
	
	@Column(value = "created_at")
	private Date   createdAt;

	public UUID getExerciseId() {
		return exerciseId;
	}

	public void setExerciseId(UUID exerciseId) {
		this.exerciseId = exerciseId;
	}

	public UUID getAttachmentId() {
		return attachmentId;
	}

	public void setAttachmentId(UUID attachmentId) {
		this.attachmentId = attachmentId;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	} 

}
