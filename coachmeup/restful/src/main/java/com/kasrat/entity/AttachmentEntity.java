package com.kasrat.entity;

import java.util.UUID;

import javax.persistence.Entity;

import org.springframework.cassandra.core.PrimaryKeyType;
import org.springframework.data.cassandra.mapping.PrimaryKeyColumn;
import org.springframework.data.cassandra.mapping.Table;

@Entity
@Table(value = "attachments")
public class AttachmentEntity {

	private UUID createdBy;
	
	@PrimaryKeyColumn(name="attachment_id", ordinal = 0, type = PrimaryKeyType.PARTITIONED)
	private UUID attachmentId;
	private String filename;
	private String location;
	private String mediaType;
	
	public UUID getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(UUID createdBy) {
		this.createdBy = createdBy;
	}
	public UUID getAttachmentId() {
		return attachmentId;
	}
	public void setAttachmentId(UUID attachmentId) {
		this.attachmentId = attachmentId;
	}
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getMediaType() {
		return mediaType;
	}
	public void setMediaType(String mediaType) {
		this.mediaType = mediaType;
	}
	
	
}
