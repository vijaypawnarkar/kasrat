package com.kasrat.entity;

import java.util.Date;
import java.util.UUID;

import javax.persistence.Entity;

import org.springframework.cassandra.core.PrimaryKeyType;
import org.springframework.data.cassandra.mapping.Column;
import org.springframework.data.cassandra.mapping.PrimaryKeyColumn;
import org.springframework.data.cassandra.mapping.Table;

@Entity
@Table(value = "user_workout_plan")
public class UserWorkoutPlanEntity {

	@PrimaryKeyColumn(name = "user_id", ordinal = 1, type = PrimaryKeyType.PARTITIONED )
	private UUID userId;
	
	@PrimaryKeyColumn(name = "plan_id", ordinal = 2, type = PrimaryKeyType.CLUSTERED )
	private UUID   planId;
	
	@Column(value = "created_at")
	private Date   createdAt; 

	public UUID getUserId() {
		return userId;
	}
	public void setUserId(UUID userId) {
		this.userId = userId;
	}

	public UUID getPlanId() {
		return planId;
	}
	public void setPlanId(UUID planId) {
		this.planId = planId;
	}
	
	public Date getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	
}
