package com.kasrat.entity;

import java.util.Date;
import java.util.UUID;

public class WorkoutSessionExerciseEntity {

	private UUID sessionId;
	private UUID planId;
	private UUID exerciseId;
	private Date startTime;
	private Date endTime;
	private UUID createdBy;
	private int numReps;
	private float weight;
	private int numSets;
	
	public UUID getSessionId() {
		return sessionId;
	}
	public void setSessionId(UUID sessionId) {
		this.sessionId = sessionId;
	}
	public UUID getPlanId() {
		return planId;
	}
	public void setPlanId(UUID planId) {
		this.planId = planId;
	}
	public UUID getExerciseId() {
		return exerciseId;
	}
	public void setExerciseId(UUID exerciseId) {
		this.exerciseId = exerciseId;
	}
	public Date getStartTime() {
		return startTime;
	}
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
	public Date getEndTime() {
		return endTime;
	}
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	public UUID getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(UUID createdBy) {
		this.createdBy = createdBy;
	}
	public int getNumReps() {
		return numReps;
	}
	public void setNumReps(int numReps) {
		this.numReps = numReps;
	}
	public float getWeight() {
		return weight;
	}
	public void setWeight(float weight) {
		this.weight = weight;
	}
	public int getNumSets() {
		return numSets;
	}
	public void setNumSets(int numSets) {
		this.numSets = numSets;
	}
	
	
}
