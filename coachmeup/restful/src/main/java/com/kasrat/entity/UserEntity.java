package com.kasrat.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;

import org.springframework.cassandra.core.PrimaryKeyType;
import org.springframework.data.cassandra.mapping.Column;
import org.springframework.data.cassandra.mapping.PrimaryKeyColumn;
import org.springframework.data.cassandra.mapping.Table;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;


@JsonInclude(Include.NON_NULL)
@Entity
@Table(value = "user")
public class UserEntity {

	@PrimaryKeyColumn(name = "user_id", ordinal = 0, type = PrimaryKeyType.PARTITIONED)
	private String userId;
	
	@PrimaryKeyColumn(name = "user_id", ordinal = 1, type = PrimaryKeyType.CLUSTERED)
	@Column(value = "email")
	private String email;

	@Column(value = "nickname")
	private String displayName;
	
	@Column(value = "auth_method")
	private String authMethod;

	@Column(value = "password")
	private String password;

	@Column(value = "first_name")
	private String firstName;
	
	@Column(value = "last_name")
	private String lastName;
	
	@Column(value = "coach")
	private boolean trainer;
	
	private String  aboutMe;
	
	private String  description;
	
	private Date lastLogin;
	private Date signupDate;
	private boolean accountActive;
	
	private String  profilePictureUrl;
	private String  profilePitcureId;
	
	private List<String> sports;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public String getDisplayName() {
		return displayName;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public boolean isTrainer() {
		return trainer;
	}

	public String getAboutMe() {
		return aboutMe;
	}

	public String getDescription() {
		return description;
	}

	public String getProfilePictureUrl() {
		return profilePictureUrl;
	}

	public String getProfilePitcureId() {
		return profilePitcureId;
	}

	public List<String> getSports() {
		return sports;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setTrainer(boolean trainer) {
		this.trainer = trainer;
	}

	public void setAboutMe(String aboutMe) {
		this.aboutMe = aboutMe;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setProfilePictureUrl(String profilePictureUrl) {
		this.profilePictureUrl = profilePictureUrl;
	}

	public void setProfilePitcureId(String profilePitcureId) {
		this.profilePitcureId = profilePitcureId;
	}

	public void setSports(List<String> sports) {
		this.sports = sports;
	}

	public Date getLastLogin() {
		return lastLogin;
	}

	public Date getSignupDate() {
		return signupDate;
	}

	public boolean isAccountActive() {
		return accountActive;
	}

	public void setLastLogin(Date lastLogin) {
		this.lastLogin = lastLogin;
	}

	public void setSignupDate(Date signupDate) {
		this.signupDate = signupDate;
	}

	public void setAccountActive(boolean accountActive) {
		this.accountActive = accountActive;
	}
	
}
