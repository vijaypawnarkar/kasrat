package com.kasrat.restful;

import java.security.Principal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.kasrat.common.SearchParameter;
import com.kasrat.entity.UserWorkoutPlanEntity;
import com.kasrat.entity.WorkoutPlanEntity;
import com.kasrat.entity.WorkoutPlanExerciseEntity;
import com.kasrat.service.WorkoutPlanService;
import com.wordnik.swagger.annotations.ApiOperation;

@Controller
@RequestMapping("/plan")
public class WorkoutPlanController {

	@Autowired
	WorkoutPlanService workoutPlanService;

	@RequestMapping( method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	@ApiOperation(value ="Get a workout plan ")
	public ResponseEntity<WorkoutPlanEntity> getWorkoutPlan(
			@RequestParam(value = "planId", required = true) String planId, Principal principal) throws Exception {
		WorkoutPlanEntity plan = workoutPlanService.getWorkoutPlan(planId);
		return new ResponseEntity<WorkoutPlanEntity>(plan, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST)
	@ApiOperation(value = "Post a workout plan")
	public WorkoutPlanEntity saveWorkoutPlan(@RequestBody WorkoutPlanEntity workoutPlan, Principal principal) throws Exception {
		return workoutPlanService.saveWorkoutPlan(workoutPlan);
	}
	
	
	@RequestMapping(value = "/search",  method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	@ApiOperation(value ="Search workout plans ")
	public ResponseEntity<List<WorkoutPlanEntity>> searchWorkoutPlans(
			@RequestParam(value = "searchStr", required = false) String searchStr, Principal principal) {

		SearchParameter param = new SearchParameter();
		param.setSearchString(searchStr);
		List<WorkoutPlanEntity> plans = workoutPlanService.findWorkoutPlans(param);
		return new ResponseEntity<List<WorkoutPlanEntity>>(plans, HttpStatus.OK);
		
	}


	@RequestMapping(value = "/createdby",  method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	@ApiOperation(value ="Get workout plans created by a user ")
	public ResponseEntity<List<WorkoutPlanEntity>> getWorkoutPlansCreatedByUserC(
			@RequestParam(value = "userId", required = false) String userId, Principal principal) {

		List<WorkoutPlanEntity> plans = workoutPlanService.getWorkoutPlansCreatedByUser(userId);
		return new ResponseEntity<List<WorkoutPlanEntity>>(plans, HttpStatus.OK);
		
	}

	@RequestMapping(value = "/likedby",  method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	@ApiOperation(value ="Get workout plans liked by a user ")
	public ResponseEntity<List<WorkoutPlanEntity>> getWorkoutPlansLikeByUserC(
			@RequestParam(value = "userId", required = false) String userId, Principal principal) {

		List<WorkoutPlanEntity> plans = workoutPlanService.getWorkoutPlansLikedByUser(userId);
		return new ResponseEntity<List<WorkoutPlanEntity>>(plans, HttpStatus.OK);		
	}

	@RequestMapping(value = "/likedby",  method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	@ApiOperation(value ="Like a workout plan ")
	public ResponseEntity<UserWorkoutPlanEntity> postWorkoutPlanLikeByUserC(
			@RequestParam(value = "userId", required = false) String userId, 
			@RequestParam(value = "planId", required = false)String planId, Principal principal ) {

		UserWorkoutPlanEntity plan = workoutPlanService.likeWorkoutPlan(userId, planId);
		return new ResponseEntity<UserWorkoutPlanEntity>(plan, HttpStatus.OK);		
	}


	@RequestMapping(value = "/exercise",  method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	@ApiOperation(value ="Get workout exercises by plan id ")
	public ResponseEntity<List<WorkoutPlanExerciseEntity>> getExercise(String exerciseId, Principal principal) {
		return new ResponseEntity<List<WorkoutPlanExerciseEntity>>(workoutPlanService.getExercises(exerciseId), HttpStatus.OK);
	}

	@RequestMapping(value = "/exercises",  method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	@ApiOperation(value ="Get workout exercises by plan id ")
	public ResponseEntity<List<WorkoutPlanExerciseEntity>> getWorkoutPlanExercises(String planId, Principal principal) {
		return new ResponseEntity<List<WorkoutPlanExerciseEntity>>(workoutPlanService.getExercises(planId), HttpStatus.OK);
	}
	
}
