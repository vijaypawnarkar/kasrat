package com.kasrat.restful;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.security.Principal;
import java.util.Date;
import java.util.UUID;

import org.apache.cassandra.utils.UUIDGen;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.kasrat.common.User;
import com.kasrat.config.FolderUtil;
import com.kasrat.entity.AttachmentEntity;
import com.kasrat.entity.WorkoutPlanAttachEntity;
import com.kasrat.entity.WorkoutPlanEntity;
import com.kasrat.entity.WorkoutPlanExerciseAttachEntity;
import com.kasrat.service.AttachmentService;
import com.kasrat.service.WorkoutPlanService;

@Controller
@RequestMapping("/attachment")
public class AttachmentController {

		@Autowired
		AttachmentService attachmentService;
	
		@Autowired
		WorkoutPlanService workoutPlanService;
		
	   @RequestMapping(value="/plan/upload", method=RequestMethod.POST)
	   public @ResponseBody String planFileUpload(@RequestParam("name") String name,
	            @RequestParam("file") MultipartFile file, @RequestParam("planId") String planId, 
	            @RequestParam("makeCover") boolean makeCover, Principal principal){
		   
	       AttachmentEntity attach = this.saveFile(name, file, principal);
		       
           WorkoutPlanAttachEntity workoutAttach = new WorkoutPlanAttachEntity();
           workoutAttach.setAttachmentId(attach.getAttachmentId());
           workoutAttach.setPlanId(UUID.fromString(planId));
           workoutAttach.setCreatedAt(new Date());
           attachmentService.saveWorkoutPlanAttach(workoutAttach);
           
           if(makeCover) {
	           try {
	        	   WorkoutPlanEntity plan = workoutPlanService.getWorkoutPlan(planId);
	        	   plan.setCoverPictureUrl(attach.getLocation());
	        	   workoutPlanService.saveWorkoutPlan(plan);
	           } catch (Exception e) {
	        	   // TODO Auto-generated catch block
	        	   e.printStackTrace();
	           }
           }
		   return "";
	    }
	   
	   @RequestMapping(value="/exercise/upload", method=RequestMethod.POST)
	   public @ResponseBody String exerciseFileUpload(@RequestParam("name") String name,
	            @RequestParam("file") MultipartFile file, @RequestParam("exerciseId") String exerciseId, 
	            @RequestParam("makeCover") boolean makeCover, Principal principal){
		   
	       AttachmentEntity attach = this.saveFile(name, file, principal);
		       
           WorkoutPlanExerciseAttachEntity exerciseAttach = new WorkoutPlanExerciseAttachEntity();
           exerciseAttach.setAttachmentId(attach.getAttachmentId());
           exerciseAttach.setExerciseId(UUID.fromString(exerciseId));
           exerciseAttach.setCreatedAt(new Date());
           attachmentService.saveWorkoutPlanExerciseAttach(exerciseAttach);
           
           if(makeCover) {
	           try {
	        	   WorkoutPlanEntity plan = workoutPlanService.getWorkoutPlan(exerciseId);
	        	   plan.setCoverPictureUrl(attach.getLocation());
	        	   workoutPlanService.saveWorkoutPlan(plan);
	           } catch (Exception e) {
	        	   // TODO Auto-generated catch block
	        	   e.printStackTrace();
	           }
           }
		   return "";
	    }
	   

	   private AttachmentEntity saveFile(String name, MultipartFile file, Principal principal) {
	   
		   	String folder = FolderUtil.getCurrentFolder();
		   	UUID attachmentId = UUIDGen.getTimeUUID();
		   	int suffixIdx = name.lastIndexOf('.');
		   	String fileLocation = folder + '/' + attachmentId + '/' + name.substring(suffixIdx+1);
		   	User activeUser = (User) ((Authentication) principal).getPrincipal();
		   	
		   	if (!file.isEmpty()) {
	            try {
	                byte[] bytes = file.getBytes();
	                BufferedOutputStream stream =
	                        new BufferedOutputStream(new FileOutputStream(new File(fileLocation)));
	                stream.write(bytes);
	                stream.close();
	                
	                AttachmentEntity attachment = new AttachmentEntity();
	                attachment.setAttachmentId(attachmentId);
	                attachment.setLocation(fileLocation);
	                attachment.setMediaType(file.getContentType());
	                attachment.setCreatedBy(activeUser.getUserId());
	                
	                AttachmentEntity attach = attachmentService.saveAttachment(attachment);
	                return attach;
	                
	            } catch (Exception e) {
	            	return null;
	            }
	        } else {
	            return null;
	        }
	   }
}
