package com.kasrat.restful;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.kasrat.entity.UserEntity;
import com.kasrat.service.PersonService;

@RestController
@RequestMapping("/profile")
public class PersonController {
	
	//@Autowired
	PersonService personService;
	
	@RequestMapping(method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<UserEntity> updateProfile(@RequestBody UserEntity entity) {
		personService.createPerson(entity);
		return new ResponseEntity<UserEntity>(entity, HttpStatus.OK);
	}

	
}
