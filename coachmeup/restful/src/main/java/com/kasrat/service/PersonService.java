package com.kasrat.service;

import com.kasrat.entity.UserEntity;

public interface PersonService {
	
	public UserEntity createPerson(UserEntity entity);
	
	public UserEntity findPerson(String personId);
	
	public boolean disableAccount(String person);

}
