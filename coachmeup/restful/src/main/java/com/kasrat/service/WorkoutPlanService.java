package com.kasrat.service;

import java.util.List;

import com.kasrat.common.SearchParameter;
import com.kasrat.entity.UserWorkoutPlanEntity;
import com.kasrat.entity.WorkoutPlanExerciseEntity;
import com.kasrat.entity.WorkoutPlanEntity;

public interface WorkoutPlanService {

	public List<WorkoutPlanEntity> findWorkoutPlans(SearchParameter parameter);

	public List<WorkoutPlanEntity> getWorkoutPlansCreatedByUser(String userId);
	
	public List<WorkoutPlanEntity> getWorkoutPlansLikedByUser(String userId);

	public UserWorkoutPlanEntity likeWorkoutPlan(String userId, String planId);

	public WorkoutPlanEntity getWorkoutPlan(String planId) throws Exception;

	public WorkoutPlanEntity saveWorkoutPlan(WorkoutPlanEntity plan) throws Exception;

	public List<WorkoutPlanExerciseEntity> getExercises(String planId);
	
	public List<WorkoutPlanExerciseEntity> findWorkoutPlansExercises(SearchParameter parameter);

	public WorkoutPlanExerciseEntity saveWorkoutPlanExercise(WorkoutPlanExerciseEntity exercise) throws Exception;
		
}
