package com.kasrat.service;



import com.kasrat.entity.AttachmentEntity;
import com.kasrat.entity.WorkoutPlanAttachEntity;
import com.kasrat.entity.WorkoutPlanExerciseAttachEntity;

public interface AttachmentService {

	public AttachmentEntity saveAttachment(AttachmentEntity attach);
	
	public WorkoutPlanAttachEntity saveWorkoutPlanAttach(WorkoutPlanAttachEntity workoutAttach) ;

	public WorkoutPlanExerciseAttachEntity saveWorkoutPlanExerciseAttach(WorkoutPlanExerciseAttachEntity workoutAttach) ;

}
