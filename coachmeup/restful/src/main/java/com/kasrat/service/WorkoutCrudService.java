package com.kasrat.service;

import com.kasrat.entity.WorkoutPlanExerciseEntity;
import com.kasrat.entity.WorkoutPlanEntity;

public interface WorkoutCrudService {
	
	void createWorkout(WorkoutPlanEntity entity);
	

	void addExercise(WorkoutPlanExerciseEntity exe);
	
	void addExercise(String workoutPlanId, String stageId, String exerciseId);
	
	void postWorkoutPicture(String workoutPlanId, byte[] picture);
	
	void postWorkoutVideo(String workoutPlanId, byte[] video);

	void rateWorkout(String workoutPlanId, int numStars, String email);
	
	void rateExercise(String exerciseId, int numStars, String email);
	
	void makeFavorite(String workoutPlanId, String email);

	
	
	
}
