package com.kasrat.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.apache.cassandra.utils.UUIDGen;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.kasrat.common.SearchParameter;
import com.kasrat.entity.UserWorkoutPlanEntity;
import com.kasrat.entity.WorkoutPlanEntity;
import com.kasrat.entity.WorkoutPlanExerciseEntity;
import com.kasrat.repo.cassandra.UserWorkoutPlanRepo;
import com.kasrat.repo.cassandra.WorkoutExerciseRepo;
import com.kasrat.repo.cassandra.WorkoutPlanRepo;
import com.kasrat.repo.solr.WorkoutPlanSearchRepo;
import com.kasrat.service.WorkoutPlanService;

@Service
public class WorkoutPlanServiceImpl implements WorkoutPlanService {
	
	@Autowired
	protected UserWorkoutPlanRepo userPlanRepo;

	@Autowired
	protected WorkoutPlanRepo planRepo;
	
	@Autowired
	protected WorkoutPlanSearchRepo planSearchRepo;

	@Autowired
	protected WorkoutExerciseRepo planExerciseRepo;
	
	/**
	 * Get a workout plan
	 */
	public WorkoutPlanEntity getWorkoutPlan(String planId) {
		List<WorkoutPlanEntity> plans =planRepo.getWorkoutPlan(UUID.fromString(planId));
		if(plans != null && plans.size() >= 0) {
			return plans.get(0);
		}
		else 
		{
			return null;
		}
	}
	
	/**
	 * Save a workout plan
	 */
	public WorkoutPlanEntity saveWorkoutPlan(WorkoutPlanEntity plan) throws Exception{
		if (StringUtils.isEmpty(plan.getPlanName()) ||
				StringUtils.isEmpty(plan.getSportName()) ||
				StringUtils.isEmpty(plan.getCreatedBy())) {
			throw new Exception("Plan Name, Sport Name, and Created By are required");
		}
		
		if(plan.getPlanId() == null) {
			UUID planId = UUIDGen.getTimeUUID();
			plan.setPlanId(planId);
		}
		return planRepo.save(plan);
	}

	/**
	 * Search Workout plans
	 */
	public List<WorkoutPlanEntity> findWorkoutPlans(SearchParameter parameter) {
		Pageable pageable = new PageRequest(parameter.getPageNumber(), parameter.getPageSize());
		List<WorkoutPlanEntity> plans = planSearchRepo.findWorkoutsByText(parameter.getSearchString(), pageable).getContent();
		return plans;
	}

	/**
	 * Find workouts created by an user
	 * @param userId
	 * @return
	 */
	public List<WorkoutPlanEntity> getWorkoutPlansCreatedByUser(String userId) {
		List<WorkoutPlanEntity> plans = planRepo.getWorkoutsCreatedByUser(userId);
		return plans;
	}

	/**
	 * Get workouts book marked/liked by an user.
	 * @param userId
	 * @return
	 */
	public List<WorkoutPlanEntity> getWorkoutPlansLikedByUser(String userId) {
		List<UserWorkoutPlanEntity> planList = userPlanRepo.getUserWorkoutPlans(userId);
		List<UUID> planIds = new ArrayList<UUID>();
		int i = 0;
		for(UserWorkoutPlanEntity plan: planList) {
			//planIds += plan.getPlanId().toString();
			planIds.add(plan.getPlanId());
			if(i < planList.size() - 1) {
				//planIds += ", ";
			}
			i++;
		}
		List<WorkoutPlanEntity> plans = planRepo.getWorkoutsByPlanIdsIn(planIds);
		return plans;
	}

	public List<WorkoutPlanExerciseEntity> getExercises(String planId) {
		return	planExerciseRepo.getExercisesByPlan(planId);
	}	
	
	
	/**
	 * Find exercises
	 */
	public List<WorkoutPlanExerciseEntity> findWorkoutPlansExercises(SearchParameter parameter) {
		return null;
	}


	public UserWorkoutPlanEntity likeWorkoutPlan(String userId, String planId) {
		
		UserWorkoutPlanEntity likePlan = new UserWorkoutPlanEntity();
		likePlan.setPlanId(UUID.fromString(planId));
		likePlan.setUserId(userId);
		return userPlanRepo.save(likePlan);
		
	}

	/**
	 * 
	 */
	public WorkoutPlanExerciseEntity saveWorkoutPlanExercise(WorkoutPlanExerciseEntity exercise) throws Exception{
		if (exercise.getPlanId() == null ||
				StringUtils.isEmpty(exercise.getExerciseName()) ||
				StringUtils.isEmpty(exercise.getCreatedBy())) {
			throw new Exception("Exercise Name, Plan Name, and Created By are required");
		}
	
		if(exercise.getExerciseId() == null) {
			UUID exerciseId = UUIDGen.getTimeUUID();
			exercise.setExerciseId(exerciseId);
		}
		return planExerciseRepo.save(exercise);
	}

}
