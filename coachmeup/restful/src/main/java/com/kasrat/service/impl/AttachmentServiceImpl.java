package com.kasrat.service.impl;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;

import com.kasrat.entity.AttachmentEntity;
import com.kasrat.entity.WorkoutPlanAttachEntity;
import com.kasrat.entity.WorkoutPlanExerciseAttachEntity;
import com.kasrat.repo.cassandra.AttachmentRepo;
import com.kasrat.repo.cassandra.WorkoutPlanAttachRepo;
import com.kasrat.repo.cassandra.WorkoutPlanExerciseAttachRepo;

public class AttachmentServiceImpl {

	@Autowired
	AttachmentRepo attachmentRepo;

	@Autowired
	WorkoutPlanAttachRepo workoutPlanAttachRepo;

	@Autowired
	WorkoutPlanExerciseAttachRepo workoutPlanExerciseAttachRepo;

	public AttachmentEntity saveAttachment(AttachmentEntity attach) {
		return attachmentRepo.save(attach);
	}
		
	public WorkoutPlanAttachEntity saveWorkoutPlanAttach(WorkoutPlanAttachEntity workoutAttach) {
		return workoutPlanAttachRepo.save(workoutAttach);
	}
	
	
	public WorkoutPlanExerciseAttachEntity saveWorkoutPlanExerciseAttach(WorkoutPlanExerciseAttachEntity workoutAttach) {
		return workoutPlanExerciseAttachRepo.save(workoutAttach);
	}
}
