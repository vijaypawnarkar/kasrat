package com.kasrat.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kasrat.entity.UserEntity;
import com.kasrat.repo.cassandra.PersonRepo;
import com.kasrat.service.PersonService;

@Service
public class PersonServiceImpl implements PersonService {
	
	//@Autowired
	PersonRepo personRepo;
	
	public UserEntity createPerson(UserEntity entity) {		
		return personRepo.save(entity);
	}

	public UserEntity findPerson(String personId) {
		return personRepo.findOne(personId);
	}

	public boolean disableAccount(String person) {
		// TODO Auto-generated method stub
		return false;
	}

}
