package com.kasrat.common;

public class SearchParameter {

	private static int DEFAULT_PAGE_SIZE=20;
	private static int DEFAULT_PAGE_NUM=0;
	private String searchString;
	private boolean allOrNone;
	private boolean exactPhrase;
	private String geoLocation;	
	private int pageSize;
	private int pageNumber;
	
	public SearchParameter() {
		pageSize = DEFAULT_PAGE_SIZE;
		pageNumber = DEFAULT_PAGE_NUM;
	}
	
	public String getSearchString() {
		return searchString;
	}
	public void setSearchString(String searchString) {
		this.searchString = searchString;
	}
	public boolean isAllOrNone() {
		return allOrNone;
	}
	public void setAllOrNone(boolean allOrNone) {
		this.allOrNone = allOrNone;
	}
	public boolean isExactPhrase() {
		return exactPhrase;
	}
	public void setExactPhrase(boolean exactPhrase) {
		this.exactPhrase = exactPhrase;
	}
	public String getGeoLocation() {
		return geoLocation;
	}
	public void setGeoLocation(String geoLocation) {
		this.geoLocation = geoLocation;
	}

	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	public int getPageNumber() {
		return pageNumber;
	}
	public void setPageNumber(int pageNumber) {
		this.pageNumber = pageNumber;
	}


}
