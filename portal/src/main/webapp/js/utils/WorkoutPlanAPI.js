var WorkoutPlanSearchActions = require('../actions/WorkoutPlanSearchActions');

module.exports = {
    
    //Search WorkoutPlans from server
    searchWorkoutPlans: function(searchStr) {
        
        var plans = [
            {planId: 'p1', planName: 'Lean mean', createdBy:'Vijay', summary: 'Leam mean machine', coverPictureUrl:'img/ex1.jpeg'},
            {planId: 'p2', planName: 'Bulk hulk', createdBy:'Vijay', summary: 'bulk hulk', coverPictureUrl:'img/ex2.jpeg'},
            {planId: 'p3', planName: 'Lean mean', createdBy:'Vijay', summary: 'Football',coverPictureUrl:'img/ex3.jpeg'},
            {planId: 'p4', planName: 'Lean mean', createdBy:'Vijay', summary: 'Leam mean machine', coverPictureUrl:'img/ex1.jpeg'},
            {planId: 'p5', planName: 'Bulk hulk', createdBy:'Vijay', summary: 'bulk hulk', coverPictureUrl:'img/ex2.jpeg'},
            {planId: 'p6', planName: 'Lean mean', createdBy:'Vijay', summary: 'Football',coverPictureUrl:'img/ex3.jpeg'}
        ];
        
        WorkoutPlanSearchActions.receiveWorkoutPlans(plans);
    },
    
    //Get my workout plans
    getFavoriteWorkoutPlans: function() {
        var plans = [
            {planId: 'p1', planName: 'Lean mean', createdBy:'Vijay', summary: 'Leam mean machine', coverPictureUrl:'img/ex1.jpeg'},
            {planId: 'p2', planName: 'Bulk hulk', createdBy:'Vijay', summary: 'bulk hulk', coverPictureUrl:'img/ex2.jpeg'},
            {planId: 'p3', planName: 'Lean mean', createdBy:'Vijay', summary: 'Football',coverPictureUrl:'img/ex3.jpeg'},
            {planId: 'p4', planName: 'Lean mean', createdBy:'Vijay', summary: 'Leam mean machine', coverPictureUrl:'img/ex1.jpeg'},
            {planId: 'p5', planName: 'Bulk hulk', createdBy:'Vijay', summary: 'bulk hulk', coverPictureUrl:'img/ex2.jpeg'},
            {planId: 'p6', planName: 'Lean mean', createdBy:'Vijay', summary: 'Football',coverPictureUrl:'img/ex3.jpeg'}
        ];
        
        WorkoutPlanSearchActions.receiveWorkoutPlans(plans);
    },
    
    //Get my workout plans
    getMyWorkoutPlans: function() {
        var plans = [
            {planId: 'p1', planName: 'Lean mean', createdBy:'Vijay', summary: 'Leam mean machine', coverPictureUrl:'img/ex1.jpeg'},
            {planId: 'p2', planName: 'Bulk hulk', createdBy:'Vijay', summary: 'bulk hulk', coverPictureUrl:'img/ex2.jpeg'},
            {planId: 'p3', planName: 'Lean mean', createdBy:'Vijay', summary: 'Football',coverPictureUrl:'img/ex3.jpeg'},
            {planId: 'p4', planName: 'Lean mean', createdBy:'Vijay', summary: 'Leam mean machine', coverPictureUrl:'img/ex1.jpeg'},
            {planId: 'p5', planName: 'Bulk hulk', createdBy:'Vijay', summary: 'bulk hulk', coverPictureUrl:'img/ex2.jpeg'},
            {planId: 'p6', planName: 'Lean mean', createdBy:'Vijay', summary: 'Football',coverPictureUrl:'img/ex3.jpeg'}
        ];
        
        WorkoutPlanSearchActions.receiveWorkoutPlans(plans);
    },
    

    //Save a workout plan
    saveWorkoutPlan: function(plan) {
    
    },
    
    //Get a workout plan
    getWorkoutPlan: function(planId) {
    
    },
    
    //Save plan as favorite
    saveAsFavorite: function(planId) {
    
    }
};