var WorkoutPlanSearchActions = require('../actions/WorkoutPlanSearchActions');

module.exports = {
    
    //Search WorkoutPlans from server
    searchWorkoutPlans: function(searchStr) {
        var plans = {};
        WorkoutPlanSearchActions.receiveWorkoutPlans(plans);
    }
    
    //Get my workout plans
    getFavoriteWorkoutPlans: function() {
        var plans = {};
        WorkoutPlanSearchActions.receiveWorkoutPlans(plans);
    }
    
    //Get my workout plans
    getMyWorkoutPlans: function() {
        var plans = {};
        WorkoutPlanSearchActions.receiveWorkoutPlans(plans);
    }
    

    //Save a workout plan
    saveWorkoutPlan: function(plan) {
    
    }
    
    //Get a workout plan
    getWorkoutPlan: function(planId) {
    
    }
    
    //Save plan as favorite
    saveAsFavorite: function(planId) {
    
    }
};