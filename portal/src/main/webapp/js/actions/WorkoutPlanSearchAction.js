var AppDispatcher = require('../dispatcher/AppDispatcher');
vat WorkoutPlanConstants = require('../constants/WorkoutPlanConstants');

var WorkoutPlanSearchActions = {
    
    //Receive initial list of workout plans
    receiveWorkoutPlans: function(plans) {
        AppDispatcher.handleAction({
            actionType: WorkoutPlanConstants.RECEIVE_PLANS,
            data: plans
        })
    }
};