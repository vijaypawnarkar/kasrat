var Button = ReactBootstrap.Button;
var Grid = ReactBootstrap.Grid;
var Row = ReactBootstrap.Row;
var Col = ReactBootstrap.Col;
var Thumbnail = ReactBootstrap.Thumbnail;
var Input = ReactBootstrap.Input;
var Pagination = ReactBootstrap.Pagination

var WorkoutExerciseRow = React.createClass({
			render: function() {
	        return (
          <div className="grid">
            <Thumbnail src={this.props.exercise.coverPictureUrl} alt='242x200'>
             <h3>{this.props.exercise.exerciseName}</h3>
             <p>{this.props.exercise.summary}</p>
             <p>
               <Button bsStyle='primary'>Like</Button>&nbsp;
               <Button bsStyle='default'>Share</Button>
             </p>
           </Thumbnail>
          </div>);
	    }
});

var WorkoutPlanExerciseList = React.createClass({
    render: function() {
      var rows = [];
      this.props.exercises.forEach(function(exercise) {
          rows.push(<WorkoutExerciseRow exercise={exercise} key={exercise.exerciseId} />);
      });
      return (
          <div className="wrapper">{rows}</div>
      );
    }
});


var WorkoutPlanDoor = React.createClass({
    render: function() {
        return (
            <div>
                <div className="jumbotran">
                  <h1>{this.props.workoutPlan.planName}</h1>
                  <p>{this.props.workoutPlan.summary}</p>
                </div>
                <Thumbnail src={this.props.workoutPlan.coverPictureUrl} alt='242x200'>
                 <p>
                   <Button bsStyle='primary'>Like</Button>&nbsp;
                   <Button bsStyle='default'>Share</Button>
                 </p>
               </Thumbnail>
            </div>
        );
    }
});

var WorkoutPlanContainer = React.createClass({
    render: function() {
        return (
            <div>
                <TopMenu />
                <div className="container-fluid">
                     <div className="span10">
                        <WorkoutPlanDoor workoutPlan={this.props.workoutPlanDetails.workoutPlan} />
                        <WorkoutPlanExerciseList exercises={this.props.workoutPlanDetails.workoutPlanExercises} />
                    </div>
              </div>
            </div>
        );
    }
});

var WORKOUT_PLAN = {workoutPlan: {planId: 'p1', planName: 'Lean mean', createdBy:'Vijay', summary: 'Leam mean machine', coverPictureUrl:'img/ex1.jpeg'},
 workoutPlanExercises: [
	{exerciseId: 'e1', exerciseName: 'Warm up', planId: 'p2', planName: 'Bulk hulk', createdBy:'Vijay', summary: 'bulk hulk', details: 'details of ex 1', numSets: 4, numReps: 10, coverImageUrl:'img/ex2.jpeg'},
  {exerciseId: 'e2', exerciseName: 'Squats ', planId: 'p2', planName: 'Bulk hulk', createdBy:'Vijay', summary: 'bulk hulk', details: 'details of ex 1', numSets: 4, numReps: 10, coverImageUrl:'img/ex2.jpeg'},
  {exerciseId: 'e3', exerciseName: 'Push ups up', planId: 'p2', planName: 'Bulk hulk', createdBy:'Vijay', summary: 'bulk hulk', details: 'details of ex 1', numSets: 4, numReps: 10, coverImageUrl:'img/ex2.jpeg'},
]};
React.render(<WorkoutPlanContainer workoutPlanDetails={WORKOUT_PLAN} />, document.body);
