var Navbar = ReactBootstrap.Navbar;
var Nav = ReactBootstrap.Nav;
var NavItem = ReactBootstrap.NavItem;
var DropdownButton = ReactBootstrap.DropdownButton;
var MenuItem = ReactBootstrap.MenuItem;

var TopMenu = React.createClass({
    render: function() {
        return <Navbar brand='Boost Me Up!'>
            <Nav>
              <NavItem eventKey={1} href='#'>Home</NavItem>
              <NavItem eventKey={2} href='#'>About</NavItem>
              <DropdownButton eventKey={3} title='Dropdown'>
                <MenuItem eventKey='1'>Action</MenuItem>
                <MenuItem eventKey='2'>Another action</MenuItem>
                <MenuItem eventKey='3'>Something else here</MenuItem>
                <MenuItem divider />
                <MenuItem eventKey='4'>Separated link</MenuItem>
              </DropdownButton>
            </Nav>
          </Navbar>;
    }
});

//React.render(<TopMenu/>, document.menu);
