
var WorkoutPlanForm = React.createClass({

    render: function() {
      return (
        <div>
        <input type="text" name="planName" value="Untitled" />

        <textarea name="summary" value="Untitled" />
        <textarea name="description" value="Untitled" />
        <button onClick={this.reset}>Reset</button>
                <button onClick={this.alertValue}>Alert Value</button>
                </div>

      );
    }
});


var WorkoutPlanEditContainer = React.createClass({
    render: function() {
        return (
            <div>
                <TopMenu />
                <div className="container-fluid">
                     <div className="span10">
                        <WorkoutPlanForm/>
                    </div>
              </div>
            </div>
        );
    }
});
React.render(<WorkoutPlanEditContainer />, document.body);
