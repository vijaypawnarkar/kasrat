var WorkoutPlanAPI = require('../utils/WorkoutPlanAPI');

var Button = ReactBootstrap.Button;
var Grid = ReactBootstrap.Grid;
var Row = ReactBootstrap.Row;
var Col = ReactBootstrap.Col;
var Thumbnail = ReactBootstrap.Thumbnail;
var Input = ReactBootstrap.Input;
var Pagination = ReactBootstrap.Pagination

//Workout plan row
var WorkoutPlanRow = React.createClass({
	render: function() {
	        return (
          <div className="grid">
            <Thumbnail src={this.props.plan.coverPictureUrl} alt='242x200'>
             <h3>{this.props.plan.planName}</h3>
             <p>{this.props.plan.summary}</p>
             <p>
               <Button bsStyle='primary'>Like</Button>&nbsp;
               <Button bsStyle='default'>Share</Button>
             </p>
           </Thumbnail>
          </div>);
    }
});

//Workout Plan List
var WorkoutPlanList = React.createClass({
	render: function() {
        var rows = [];
        var lastCategory = null;
        this.props.plans.forEach(function(plan) {
            rows.push(<WorkoutPlanRow plan={plan} key={plan.planId} />);
        });
        return (
            <div className="wrapper">{rows}</div>
        );
    }
	}
);

//Search bar
var SearchBar = React.createClass({
  getInitialState() {
      return {
        value: ''
      };
    },

    validationState() {
      var length = this.state.value.length;
      if (length > 10) { return 'success'; }
      else if (length > 5) { return 'warning'; }
      else if (length > 0) { return 'error'; }
    },

    handleChange() {
      // This could also be done using ReactLink:
      // http://facebook.github.io/react/docs/two-way-binding-helpers.html
      this.setState({
        value: this.refs.input.getValue()
      });
    },

    render() {
      return (
        <Row>
      <Col xs={3}>
        <Input
          type='text'
          value={this.state.value}
          placeholder='Enter text'
          label='Working example with validation'
          help='Validation is based on string length.'
          bsStyle={this.validationState()}
          hasFeedback
          ref='input'
          groupClassName='group-class'
          labelClassName='label-class'
          onChange={this.handleChange} />
      </Col>
      </Row>
      );
    }
});

//Pagination
var PaginationAdvanced = React.createClass({
  getInitialState() {
    return {
      activePage: 1
    };
  },

  handleSelect(event, selectedEvent) {
    this.setState({
      activePage: selectedEvent.eventKey
    });
  },

  render() {
    return (
      <Pagination
        prev
        next
        first
        last
        ellipsis
        items={20}
        maxButtons={5}
        activePage={this.state.activePage}
        onSelect={this.handleSelect} />
    );
  }
});

//Container  (Search UI + Pagination + WorkoutPlan list)
var FilterableWorkoutPlanTable = React.createClass({

    getInitialState: function() {
	  return getWorkoutPlans();
	},

	componentDidMount: function() {
      WorkoutPlanStore.addChangeListener(this._onChange);
	},

    componentWillUnmount: function() {
	  WorkoutPlanStore.removeChangeListener(this._onChange);
    },

    render: function() {
        return (
            <div>
                <TopMenu />
                <div className="container-fluid">
                     <div className="span10">
                        <SearchBar />
                        <WorkoutPlanList plans={this.state.plans} />
                        <PaginationAdvanced />
                    </div>
              </div>
            </div>
        );
    },

    _onChange: function() {
	  this.setState(getWorkoutPlansStates());
    }
});

WorkoutPlanAPI.searchWorkoutPlans();

React.render(<FilterableWorkoutPlanTable />, document.body);
