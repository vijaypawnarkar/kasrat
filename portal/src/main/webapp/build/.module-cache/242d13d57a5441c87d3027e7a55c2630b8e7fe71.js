
var WorkoutPlanForm = React.createClass({displayName: "WorkoutPlanForm",

    render: function() {
      return (
        React.createElement("div", null, 
        React.createElement("input", {type: "text", name: "planName", value: "Untitled"}), 
        React.createElement("textarea", {name: "summary", value: "Untitled"}), 
        React.createElement("textarea", {name: "description", value: "Untitled"}), 
        React.createElement("button", {onClick: this.reset}, "Reset"), 
                React.createElement("button", {onClick: this.alertValue}, "Alert Value")
                )

      );
    }
});
