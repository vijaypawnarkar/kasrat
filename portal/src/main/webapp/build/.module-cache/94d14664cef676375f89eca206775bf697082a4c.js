var AppDispatcher = require('../dispatcher/AppDispatcher');

var CHANGE_EVENT = 'change';

var _plans = {}; //List of plans

function createPlan(plan) {
  _plans[plan.planId] = plan;
}

var WorkoutPlanStore = assign({}, EventEmitter.prototype, {

  /**
   * Get the entire collection of plans.
   * @return {object}
   */
  getAll: function() {
    return _plans;
  },

  emitChange: function() {
    this.emit(CHANGE_EVENT);
  },

  /**
   * @param {function} callback
   */
  addChangeListener: function(callback) {
    this.on(CHANGE_EVENT, callback);
  },

  /**
   * @param {function} callback
   */
  removeChangeListener: function(callback) {
    this.removeListener(CHANGE_EVENT, callback);
  },

  dispatcherIndex: AppDispatcher.register(function(payload) {
    var action = payload.action;
    var plan;

    switch(action.actionType) {
      case WorkPlanConstants.PLAN_CREATE:
        plan = action.plan;
        if (plan !== '') {
          create(text);
          WorkoutPlanStore.emitChange();
        }
        break;
      case WorkPlanConstants.PLAN_SAVE:
          plan = action.plan;
          if (plan !== '') {
            create(text);
            WorkoutPlanStore.emitChange();
          }
          break;

      case WorkPlanConstants.PLAN_DELETE:
        destroy(action.id);
        WorkoutPlanStore.emitChange();
        break;

      // add more cases for other actionTypes, like TODO_UPDATE, etc.
    }

    return true; // No errors. Needed by promise in Dispatcher.
  })

});

module.exports = WorkoutPlanStore;
