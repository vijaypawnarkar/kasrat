
var WorkoutPlanForm = React.createClass({displayName: "WorkoutPlanForm",

    render: function() {
      return (
        React.createElement("div", null, 
        React.createElement("input", {type: "text", name: "planName", value: "Untitled"}), 
        React.createElement("textarea", {name: "summary", value: "Untitled"}), 
        React.createElement("textarea", {name: "description", value: "Untitled"}), 
        React.createElement("button", {onClick: this.reset}, "Reset"), 
                React.createElement("button", {onClick: this.alertValue}, "Alert Value")
                )

      );
    }
});



var WorkoutPlanEditContainer = React.createClass({displayName: "WorkoutPlanEditContainer",
    render: function() {
        return (
            React.createElement("div", null, 
                React.createElement(TopMenu, null), 
                React.createElement("div", {className: "container-fluid"}, 
                     React.createElement("div", {className: "span10"}, 
                        React.createElement(WorkoutPlanForm, null)
                    )
              )
            )
        );
    }
});
React.render(React.createElement(WorkoutPlanEditContainer, null), document.body);
