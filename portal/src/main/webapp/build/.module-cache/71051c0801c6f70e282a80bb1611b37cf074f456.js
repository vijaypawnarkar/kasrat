var FavoriteButton = React.createClass({displayName: "FavoriteButton",
    render: function() {
        return
          React.createElement("div", null, 
            React.createElement(ButtonToolbar, null, 
              React.createElement(Button, {bsSize: "large"}, "Large button")
           )
         )
    }
});

var WorkoutPlanImage = React.createClass({displayName: "WorkoutPlanImage",
    render: function() {
        return React.createElement("div", {class: "responsive-container"}, 
							  	React.createElement("div", {class: "dummy"}), 
								  React.createElement("div", {class: "img-container"}, 
			 						  	React.createElement("img", {src: this.props.coverPictureUrl, alt: ""})
	 							  )
        			)
    }
});

var WorkoutPlanRow = React.createClass({displayName: "WorkoutPlanRow",

			render: function() {
	        return React.createElement("div", null, 
							React.createElement(WorkoutPlanImage, {coverPictureUrl: this.props.plan.coverPictureUrl}), 
						  React.createElement("div", null, this.props.plan.coverPictureUrl), 
	            React.createElement("div", null, this.props.plan.planName), 
							React.createElement("div", null, this.props.plan.createdBy), 
							React.createElement("div", null, this.props.plan.summary), 
 							React.createElement(FavoriteButton, {text: "Like", icon: "fa-arrow-circle-o-up"}
							 )
							);
	    }
});

var WorkoutPlanList = React.createClass({displayName: "WorkoutPlanList",
	render: function() {
        var rows = [];
        var lastCategory = null;
        this.props.plans.forEach(function(plan) {
            rows.push(React.createElement(WorkoutPlanRow, {plan: plan, key: plan.planId}));
        });
        return (
            React.createElement("table", null, 
                React.createElement("thead", null, 
                    React.createElement("tr", null, 
                        React.createElement("th", null, "Name"), 
                        React.createElement("th", null, "Price")
                    )
                ), 
                React.createElement("tbody", null, rows)
            )
        );
    }
	}
);


var SearchBar = React.createClass({displayName: "SearchBar",
    render: function() {
        return (
            React.createElement("form", null, 
                React.createElement("input", {type: "text", placeholder: "Search..."}), 
                React.createElement("p", null, 
                    React.createElement("input", {type: "checkbox"}), 
                    ' ', 
                    "Only show my workout plans"
                )
            )
        );
    }
});

var FilterableWorkoutPlanTable = React.createClass({displayName: "FilterableWorkoutPlanTable",
    render: function() {
        return (
            React.createElement("div", null, 
                React.createElement(SearchBar, null), 
                React.createElement(WorkoutPlanList, {plans: this.props.plans})
            )
        );
    }
});


var PLANS = [
  {planId: 'p1', planName: 'Lean mean', createdBy:'Vijay', summary: 'Leam mean machine', coverPictureUrl:'z'},
	{planId: 'p2', planName: 'Bulk hulk', createdBy:'Vijay', summary: 'bulk hulk', coverPictureUrl:'x'},
  {planId: 'p3', planName: 'Lean mean', createdBy:'Vijay', summary: 'Football',coverPictureUrl:'y'}
];

React.render(React.createElement(FilterableWorkoutPlanTable, {plans: PLANS}), document.body);

var Alert = ReactBootstrap.Alert;
