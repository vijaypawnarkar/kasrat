var Navbar = ReactBootstrap.Navbar;
var Nav = ReactBootstrap.Nav;
var NavItem = ReactBootstrap.NavItem;
var DropdownButton = ReactBootstrap.DropdownButton;
var MenuItem = ReactBootstrap.MenuItem;

var TopMenu = React.createClass({displayName: "TopMenu",
    render: function() {
        return React.createElement(Navbar, {brand: "Boost Me Up!"}, 
            React.createElement(Nav, null, 
              React.createElement(NavItem, {eventKey: 1, href: "#"}, "Home"), 
              React.createElement(NavItem, {eventKey: 2, href: "#"}, "About"), 
              React.createElement(DropdownButton, {eventKey: 3, title: "Dropdown"}, 
                React.createElement(MenuItem, {eventKey: "1"}, "Action"), 
                React.createElement(MenuItem, {eventKey: "2"}, "Another action"), 
                React.createElement(MenuItem, {eventKey: "3"}, "Something else here"), 
                React.createElement(MenuItem, {divider: true}), 
                React.createElement(MenuItem, {eventKey: "4"}, "Separated link")
              )
            )
          );
    }
});

//React.render(<TopMenu/>, document.menu);
