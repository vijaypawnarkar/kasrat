var Button = ReactBootstrap.Button;
var Grid = ReactBootstrap.Grid;
var Row = ReactBootstrap.Row;
var Col = ReactBootstrap.Col;
var Thumbnail = ReactBootstrap.Thumbnail;
var Input = ReactBootstrap.Input;
var Pagination = ReactBootstrap.Pagination


var WorkoutPlanRow = React.createClass({displayName: "WorkoutPlanRow",
			render: function() {
	        return (
          React.createElement("div", {className: "span4"}, 
            React.createElement(Thumbnail, {src: this.props.plan.coverPictureUrl, alt: "242x200"}, 
             React.createElement("h3", null, this.props.plan.planName), 
             React.createElement("p", null, this.props.plan.summary), 
             React.createElement("p", null, 
               React.createElement(Button, {bsStyle: "primary"}, "Like"), " ", 
               React.createElement(Button, {bsStyle: "default"}, "Share")
             )
           )
          ));
	    }
});

var WorkoutPlanList = React.createClass({displayName: "WorkoutPlanList",
	render: function() {
        var rows = [];
        var lastCategory = null;
        this.props.plans.forEach(function(plan) {
            rows.push(React.createElement(WorkoutPlanRow, {plan: plan, key: plan.planId}));
        });
        return (
            React.createElement("div", {className: "container-fluid"}, rows)
        );
    }
	}
);


var SearchBar = React.createClass({displayName: "SearchBar",
  getInitialState() {
      return {
        value: ''
      };
    },

    validationState() {
      var length = this.state.value.length;
      if (length > 10) { return 'success'; }
      else if (length > 5) { return 'warning'; }
      else if (length > 0) { return 'error'; }
    },

    handleChange() {
      // This could also be done using ReactLink:
      // http://facebook.github.io/react/docs/two-way-binding-helpers.html
      this.setState({
        value: this.refs.input.getValue()
      });
    },

    render() {
      return (
        React.createElement(Row, null, 
      React.createElement(Col, {xs: 3}, 
        React.createElement(Input, {
          type: "text", 
          value: this.state.value, 
          placeholder: "Enter text", 
          label: "Working example with validation", 
          help: "Validation is based on string length.", 
          bsStyle: this.validationState(), 
          hasFeedback: true, 
          ref: "input", 
          groupClassName: "group-class", 
          labelClassName: "label-class", 
          onChange: this.handleChange})
      )
      )
      );
    }
});


var PaginationAdvanced = React.createClass({displayName: "PaginationAdvanced",
  getInitialState() {
    return {
      activePage: 1
    };
  },

  handleSelect(event, selectedEvent) {
    this.setState({
      activePage: selectedEvent.eventKey
    });
  },

  render() {
    return (
      React.createElement(Pagination, {
        prev: true, 
        next: true, 
        first: true, 
        last: true, 
        ellipsis: true, 
        items: 20, 
        maxButtons: 5, 
        activePage: this.state.activePage, 
        onSelect: this.handleSelect})
    );
  }
});


var FilterableWorkoutPlanTable = React.createClass({displayName: "FilterableWorkoutPlanTable",
    render: function() {
        return (
            React.createElement("div", null, 
                React.createElement(TopMenu, null), 
                React.createElement("div", {className: "container-fluid"}, 
                  React.createElement("div", {className: "row-fluid"}, 
                     React.createElement("div", {className: "span2"}
                     ), 
                     React.createElement("div", {className: "span10"}, 
                        React.createElement(SearchBar, null), 
                        React.createElement(WorkoutPlanList, {plans: this.props.plans}), 
                        React.createElement(PaginationAdvanced, null)
                    )
                 )
              )
            )
        );
    }
});


var PLANS = [
  {planId: 'p1', planName: 'Lean mean', createdBy:'Vijay', summary: 'Leam mean machine', coverPictureUrl:'img/ex1.jpeg'},
	{planId: 'p2', planName: 'Bulk hulk', createdBy:'Vijay', summary: 'bulk hulk', coverPictureUrl:'img/ex2.jpeg'},
  {planId: 'p3', planName: 'Lean mean', createdBy:'Vijay', summary: 'Football',coverPictureUrl:'img/ex3.jpeg'}
];

React.render(React.createElement(FilterableWorkoutPlanTable, {plans: PLANS}), document.body);
