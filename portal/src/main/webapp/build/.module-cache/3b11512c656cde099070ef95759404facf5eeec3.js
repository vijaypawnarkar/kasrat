

var WorkoutPlanContainer = React.createClass({displayName: "WorkoutPlanContainer",
    render: function() {
        return (
            React.createElement("div", null, 
                React.createElement(TopMenu, null), 
                React.createElement("div", {className: "container-fluid"}, 
                     React.createElement("div", {className: "span10"}, 
                        React.createElement(WorkoutPlan, {workoutPlan: this.props.workoutPlanDetails.workoutPlan}), 
                        React.createElement(PaginationAdvanced, null)
                    )
              )
            )
        );
    }
});
React.render(React.createElement(WorkoutPlanContainer, {workoutPlanDetails: WORKOUT_PLAN}), document.body);

var WORKOUT_PLAN = {workoutPlan: {planId: 'p1', planName: 'Lean mean', createdBy:'Vijay', summary: 'Leam mean machine', coverPictureUrl:'img/ex1.jpeg'},
 workoutPlanExercise: [
	{exerciseId: 'e1', exerciseName: 'Warm up', planId: 'p2', planName: 'Bulk hulk', createdBy:'Vijay', summary: 'bulk hulk', details: 'details of ex 1', numSets: 4, numReps: 10, coverImageUrl:'img/ex2.jpeg'},
  {exerciseId: 'e2', exerciseName: 'Squats ', planId: 'p2', planName: 'Bulk hulk', createdBy:'Vijay', summary: 'bulk hulk', details: 'details of ex 1', numSets: 4, numReps: 10, coverImageUrl:'img/ex2.jpeg'},
  {exerciseId: 'e3', exerciseName: 'Push ups up', planId: 'p2', planName: 'Bulk hulk', createdBy:'Vijay', summary: 'bulk hulk', details: 'details of ex 1', numSets: 4, numReps: 10, coverImageUrl:'img/ex2.jpeg'},
]};
