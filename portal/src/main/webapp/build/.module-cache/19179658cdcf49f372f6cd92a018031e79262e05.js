var Button = ReactBootstrap.Button;
var Grid = ReactBootstrap.Grid;
var Row = ReactBootstrap.Row;
var Col = ReactBootstrap.Col;
var Thumbnail = ReactBootstrap.Thumbnail;
var Input = ReactBootstrap.Input;
var Pagination = ReactBootstrap.Pagination

var WorkoutExerciseRow = React.createClass({displayName: "WorkoutExerciseRow",
			render: function() {
	        return (
          React.createElement("div", {className: "grid"}, 
            React.createElement(Thumbnail, {src: this.props.exercise.coverPictureUrl, alt: "242x200"}, 
             React.createElement("h3", null, this.props.exercise.exerciseName), 
             React.createElement("p", null, this.props.exercise.summary), 
             React.createElement("p", null, 
               React.createElement(Button, {bsStyle: "primary"}, "Like"), " ", 
               React.createElement(Button, {bsStyle: "default"}, "Share")
             )
           )
          ));
	    }
});

var WorkoutPlanExerciseList = React.createClass({displayName: "WorkoutPlanExerciseList",
    render: function() {
      var rows = [];
      this.props.exercises.forEach(function(exercise) {
          rows.push(React.createElement(WorkoutExerciseRow, {exercise: exercise, key: exercise.exerciseId}));
      });
      return (
          React.createElement("div", {className: "wrapper"}, rows)
      );
    }
});


var WorkoutPlanDoor = React.createClass({displayName: "WorkoutPlanDoor",
    render: function() {
        return (
            React.createElement("div", null, 
                React.createElement("div", {className: "container-fluid"}, 
                React.createElement("div", {className: "grid"}, 
                  React.createElement(Thumbnail, {src: this.props.workoutPlan.coverPictureUrl, alt: "242x200"}, 
                   React.createElement("h3", null, this.props.workoutPlan.planName), 
                   React.createElement("p", null, this.props.workoutPlan.summary), 
                   React.createElement("p", null, 
                     React.createElement(Button, {bsStyle: "primary"}, "Like"), " ", 
                     React.createElement(Button, {bsStyle: "default"}, "Share")
                   )
                 )
                ), ");"
              )
            )
        );
    }
});

var WorkoutPlanContainer = React.createClass({displayName: "WorkoutPlanContainer",
    render: function() {
        return (
            React.createElement("div", null, 
                React.createElement(TopMenu, null), 
                React.createElement("div", {className: "container-fluid"}, 
                     React.createElement("div", {className: "span10"}, 
                        React.createElement(WorkoutPlanDoor, {workoutPlan: this.props.workoutPlanDetails.workoutPlan}), 
                        React.createElement(WorkoutPlanExerciseList, {exercises: this.props.workoutPlanDetails.workoutPlanExercises})
                    )
              )
            )
        );
    }
});

var WORKOUT_PLAN = {workoutPlan: {planId: 'p1', planName: 'Lean mean', createdBy:'Vijay', summary: 'Leam mean machine', coverPictureUrl:'img/ex1.jpeg'},
 workoutPlanExercises: [
	{exerciseId: 'e1', exerciseName: 'Warm up', planId: 'p2', planName: 'Bulk hulk', createdBy:'Vijay', summary: 'bulk hulk', details: 'details of ex 1', numSets: 4, numReps: 10, coverImageUrl:'img/ex2.jpeg'},
  {exerciseId: 'e2', exerciseName: 'Squats ', planId: 'p2', planName: 'Bulk hulk', createdBy:'Vijay', summary: 'bulk hulk', details: 'details of ex 1', numSets: 4, numReps: 10, coverImageUrl:'img/ex2.jpeg'},
  {exerciseId: 'e3', exerciseName: 'Push ups up', planId: 'p2', planName: 'Bulk hulk', createdBy:'Vijay', summary: 'bulk hulk', details: 'details of ex 1', numSets: 4, numReps: 10, coverImageUrl:'img/ex2.jpeg'},
]};
React.render(React.createElement(WorkoutPlanContainer, {workoutPlanDetails: WORKOUT_PLAN}), document.body);
