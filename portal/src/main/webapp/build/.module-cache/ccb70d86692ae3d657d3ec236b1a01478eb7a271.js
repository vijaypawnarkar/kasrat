var Button = ReactBootstrap.Button;
var Grid = ReactBootstrap.Grid;
var Row = ReactBootstrap.Row;
var Col = ReactBootstrap.Col;
var Thumbnail = ReactBootstrap.Thumbnail;

var WorkoutPlanRow = React.createClass({displayName: "WorkoutPlanRow",

			render: function() {
	        return React.createElement(Row, null, 
          React.createElement(Col, {xs: 6, md: 4}, 
            React.createElement(Thumbnail, {src: this.props.plan.coverPictureUrl, alt: "242x200"}, 
             React.createElement("h3", null, this.props.plan.planName), 
             React.createElement("p", null, this.props.plan.summary), 
             React.createElement("p", null, 
               React.createElement(Button, {bsStyle: "primary"}, "Like"), " ", 
               React.createElement(Button, {bsStyle: "default"}, "Share")
             )
           )
          )
          );
	    }
});

var WorkoutPlanList = React.createClass({displayName: "WorkoutPlanList",
	render: function() {
        var rows = [];
        var lastCategory = null;
        this.props.plans.forEach(function(plan) {
            rows.push(React.createElement(WorkoutPlanRow, {plan: plan, key: plan.planId}));
        });
        return (
            React.createElement(Grid, null, rows)
        );
    }
	}
);


var SearchBar = React.createClass({displayName: "SearchBar",
    render: function() {
        return (
            React.createElement("form", null, 
                React.createElement("input", {type: "text", placeholder: "Search..."}), 
                React.createElement("p", null, 
                    React.createElement("input", {type: "checkbox"}), 
                    ' ', 
                    "Only show my workout plans"
                )
            )
        );
    }
});

var FilterableWorkoutPlanTable = React.createClass({displayName: "FilterableWorkoutPlanTable",
    render: function() {
        return (
            React.createElement("div", null, 
                React.createElement(SearchBar, null), 
                React.createElement(WorkoutPlanList, {plans: this.props.plans})
            )
        );
    }
});


var PLANS = [
  {planId: 'p1', planName: 'Lean mean', createdBy:'Vijay', summary: 'Leam mean machine', coverPictureUrl:'z'},
	{planId: 'p2', planName: 'Bulk hulk', createdBy:'Vijay', summary: 'bulk hulk', coverPictureUrl:'x'},
  {planId: 'p3', planName: 'Lean mean', createdBy:'Vijay', summary: 'Football',coverPictureUrl:'y'}
];

React.render(React.createElement(FilterableWorkoutPlanTable, {plans: PLANS}), document.body);

var Alert = ReactBootstrap.Alert;
