package com.kasrat.microservices.common;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;


public class ImageProperties {

	private static Properties props = new Properties();
	private static List<Integer> heights;
	private static List<Integer> widths;
	
	public static void loadProperties() {
		synchronized (props) {
			props.clear();
			try {
				props.load(ImageProperties.class.getResourceAsStream("image.properties"));
				heights = getHeightList();
				widths = getWidthList();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}


	public static List<Integer> getHeightList() {
		return heights;
	}

	public static List<Integer> getWidthList() {
		return widths;
	}
	
	public static List<Integer> loadHeightList() {
		String sizes = props.getProperty("image.sizes.widths");
		return getSizes(sizes);
	}

	public static List<Integer> loadWidthList() {
		String sizes = props.getProperty("image.sizes.widths");
		return getSizes(sizes);
	}

	private static List<Integer> getSizes(String sizes) {
		String[] sizeArr = sizes.split(",");
		List<Integer> sizeList = new ArrayList<Integer>();
		for(int i=0; i< sizeArr.length; i++) {
			sizeList.add(Integer.valueOf(sizeArr[i]));
		}
		return sizeList;
	}

}
