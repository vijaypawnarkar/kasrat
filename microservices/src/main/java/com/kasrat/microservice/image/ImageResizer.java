package com.kasrat.microservice.image;

import java.util.List;

import javaxt.io.Image;

import com.kasrat.microservices.common.ImageProperties;

public class ImageResizer {

	public void generateImages(String image, String folder) {
		
		Image originalImg = new Image(folder + image);
		List<Integer> heights = ImageProperties.getHeightList();
		for(Integer ht: heights) {
			Image copyImg = originalImg.copy();
			copyImg.setHeight(ht);
			copyImg.saveAs(folder + "/h/" + ht + "/" + image);
		}
		
		List<Integer> widths = ImageProperties.getWidthList();
		for(Integer wd: widths) {
			Image copyImg = originalImg.copy();
			copyImg.setHeight(wd);
			copyImg.saveAs(folder + "/w/" + wd + "/" + image);
		}
		
	}
}
