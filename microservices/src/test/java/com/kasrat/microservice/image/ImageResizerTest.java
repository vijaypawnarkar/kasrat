package com.kasrat.microservice.image;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import javaxt.io.Image;

import com.kasrat.microservices.common.ImageProperties;

public class ImageResizerTest {
	
	public static void main(String [] r) {
		ImageResizerTest t = new ImageResizerTest();
		t.generateImagesTest();
	}

	@Test
	public void generateImagesTest() {
		
		String folder = "/Users/vijaypawnarkar/Desktop/";
		String image = "IMG_0089.JPG";
		
		List<Integer> heights = new ArrayList<Integer>();
		heights.add(40);
		heights.add(100);
		heights.add(400);
		Image originalImg = new Image(folder + image);
		for(Integer ht: heights) {
			Image copyImg = originalImg.copy();
			copyImg.setHeight(ht);
			copyImg.saveAs(folder + "/h/" + ht + "/" + image);
		}
		
		for(Integer ht: heights) {
			Image copyImg = originalImg.copy();
			copyImg.setHeight(ht);
			copyImg.saveAs(folder + "/w/" + ht + "/" + image);
		}
		

	}
}
